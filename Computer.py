from General_functions import make_new_tree, find_depth, evaluate_board


def get_computers_turn(board, alpha, beta, stage1):
    depth = find_depth(board, stage1)
    tree = make_new_tree(board, depth, stage1)
    next_move, utility = minimax(tree.root, "B", alpha, beta, stage1)
    board.array = next_move.board
    return board


def minimax(node, player, alpha, beta, stage1):
    final_board = None
    if len(node.get_children()) == 0:
        return final_board, evaluate_board(node, stage1)
    for possible_board in node.get_children():
        if player == "W":
            current_board, current_evaluation = minimax(possible_board, "B", alpha, beta, stage1)
            if current_evaluation > alpha:
                final_board = possible_board
                alpha = current_evaluation
        else:
            current_board, current_evaluation = minimax(possible_board, "W", alpha, beta, stage1)
            if current_evaluation < beta:
                final_board = possible_board
                beta = current_evaluation
        if alpha >= beta:
            break
    if player == "W":
        final_evaluation = alpha
    else:
        final_evaluation = beta
    return final_board, final_evaluation
