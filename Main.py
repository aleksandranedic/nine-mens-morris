from Mill import *
from Computer import get_computers_turn
from General_functions import remove_figurine, is_end
from HashMap import HashMap, hes
from math import inf

alpha = -inf
beta = inf
if __name__ == '__main__':
    board = HashMap()
    for i in range(9):
        print(board)
        while True:
            board_position = input("Molimo Vas unesite gde zelite da stavite figuru: ")
            try:
                if len(board_position) == 2 and 0 <= int(board_position[0]) < 8 and 0 <= int(board_position[1]) < 3:
                    if board.get_piece(board_position) == 0:
                        break
                    else:
                        print("Mesto je vec zauzeto, izaberite drugo: ")
                else:
                    print("Niste uneli poziciju u opsegu 0-7,0-2 ")
            except ValueError:
                print("Niste uneli broj kao mogucu poziciju, probajte ponovo: ")
        board.add_piece(board_position, "W")
        if is_mill(board.array, "W", hes(board_position)):
            board = remove_figurine(board)
        board = get_computers_turn(board, alpha, beta, True)

    stage3_for_W = False
    while True:
        print(board)
        while True:
            move_from_pos = input("Izaberite kojoj figuri zelite da menjate mesto: ")
            try:
                if len(move_from_pos) == 2 and 0 <= int(move_from_pos[0]) < 8 and 0 <= int(move_from_pos[1]) < 3:
                    if board.get_piece(move_from_pos) == "W":
                        if stage3_for_W:
                            break
                        if not no_free_position_left(board.array, hes(move_from_pos)):
                            break
                    else:
                        print("Niste izabrali svoju figuru, probajte ponovo: ")
                else:
                    print("Niste uneli poziciju u opsegu 0-7,0-2 ")
            except ValueError:
                print("Niste uneli broj kao mogucu poziciju, probajte ponovo:")
        while True:
            move_to_pos = input("Izaberite mesto gde zelite da stavite figuricu : ")
            try:
                if len(move_to_pos) == 2 and 0 <= int(move_to_pos[0]) < 8 and 0 <= int(move_to_pos[1]) < 3:
                    if board.get_piece(move_to_pos) == 0:
                        if stage3_for_W:
                            break
                        if accessible_position(hes(move_from_pos), hes(move_to_pos)):
                            break
                    else:
                        print("Niste izabrali vazecu poziciju, probajte ponovo: ")
                else:
                    print("Niste uneli poziciju u opsegu 0-7,0-2 ")
            except ValueError:
                print("Niste uneli broj kao mogucu poziciju, probajte ponovo:")
        board.delete_piece(move_from_pos)
        board.add_piece(move_to_pos, "W")
        if is_mill(board.array, "W", hes(move_to_pos)):
            board = remove_figurine(board)
        if is_end(board.array, "B"):
            print("Beli igrac je pobedio!")
            break
        board = get_computers_turn(board, alpha, beta, False)
        if number_of_pieces_on_board(board.array, "W") == 3:
            stage3_for_W = True
        if is_end(board.array, "W"):
            print("Crni igrac je pobedio!")
            break
    print(board)
