import copy


def nextMove(player, move, string_board):
    board_list = make_list_board(string_board)
    board = HashMap()
    board.array = board_list
    if move == "INIT":
        stage1 = True
        new_board = ai_turn_to_choose_spot(copy.deepcopy(board), -10000, 10000, stage1, player)
    elif move == "MOVE":
        stage1 = False
        new_board = ai_turn_to_choose_spot(copy.deepcopy(board), -10000, 10000, stage1, player)
    else:
        new_board = removing_opponent_piece(copy.deepcopy(board.array), change_player(player))
        return find_key_from_mill(new_board, board.array)
    return find_key(new_board.array, board.array, stage1, player)


def find_key(new_board, old_board, stage1, player):
    for i in range(len(new_board)):
        if new_board[i] != old_board[i] and old_board[i] != change_player(player):
            if stage1:
                return transform_to_string(i)
            else:
                if old_board[i] == player and new_board[i] == "O":
                    sklonjena_sa_polja = transform_to_string(i)
                elif old_board[i] == "O" and new_board[i] == player:
                    stavljena_na_polje = transform_to_string(i)
    return sklonjena_sa_polja + " " + stavljena_na_polje


def find_key_from_mill(new_board, old_board):
    for i in range(len(new_board)):
        if new_board[i] != old_board[i]:
            return transform_to_string(i)


def transform_to_string(number):
    if number == 0:
        return "0 0"
    elif number == 1:
        return "0 3"
    elif number == 2:
        return "0 6"
    elif number == 3:
        return "1 1"
    elif number == 4:
        return "1 3"
    elif number == 5:
        return "1 5"
    elif number == 6:
        return "2 2"
    elif number == 7:
        return "2 3"
    elif number == 8:
        return "2 4"
    elif number == 9:
        return "3 0"
    elif number == 10:
        return "3 1"
    elif number == 11:
        return "3 2"
    elif number == 12:
        return "3 4"
    elif number == 13:
        return "3 5"
    elif number == 14:
        return "3 6"
    elif number == 15:
        return "4 2"
    elif number == 16:
        return "4 3"
    elif number == 17:
        return "4 4"
    elif number == 18:
        return "5 1"
    elif number == 19:
        return "5 3"
    elif number == 20:
        return "5 5"
    elif number == 21:
        return "6 0"
    elif number == 22:
        return "6 3"
    elif number == 23:
        return "6 6"


def make_list_board(board):
    new_board = []
    for item in board:
        if item == "W" or item == "B" or item == "O":
            new_board.append(item)
    return new_board


def removing_opponent_piece(board, removing_this_player):
    if removing_this_player == "B":
        best_option = -10000
    else:
        best_option = 10000
    for i in range(len(board)):
        if board[i] == removing_this_player:
            new_board = copy.deepcopy(board)
            new_board[i] = "O"
            node = Node(new_board)
            current_option = evaluate_board(node, True)
            if removing_this_player == "B":
                if current_option > best_option:
                    best_option = current_option
                    best_move = new_board
            else:
                if current_option < best_option:
                    best_option = current_option
                    best_move = new_board
    return best_move


def ai_turn_to_choose_spot(board, alpha, beta, stage1, player):
    depth = find_depth(board, stage1)
    tree = make_new_tree(board, depth, stage1, change_player(player))
    next_move, utility = minimax(tree.root, player, alpha, beta, stage1)
    board.array = next_move.board
    return board


def minimax(node, player, alpha, beta, stage1):
    final_board = None
    if len(node.get_children()) == 0:
        return final_board, evaluate_board(node, stage1)
    for possible_board in node.get_children():
        if player == "W":
            current_board, current_evaluation = minimax(possible_board, "B", alpha, beta, stage1)
            if current_evaluation > alpha:
                final_board = possible_board
                alpha = current_evaluation
        else:
            current_board, current_evaluation = minimax(possible_board, "W", alpha, beta, stage1)
            if current_evaluation < beta:
                final_board = possible_board
                beta = current_evaluation
        if alpha >= beta:
            break
    if player == "W":
        final_evaluation = alpha
    else:
        final_evaluation = beta
    return final_board, final_evaluation


def remove_figurine(board):
    while True:
        board_position = input("Molimo Vas unesite poziciju figure koju zelite da sklonite: ")
        try:
            if len(board_position) == 2 and 0 <= int(board_position[0]) < 8 and 0 <= int(board_position[1]) < 3:
                if board.get_piece(board_position) == "B":
                    if all_are_mill(board.array, "B"):
                        break
                    elif not is_mill(board.array, "B", hes(board_position)):
                        break
                else:
                    print("Nevazece mesto, probajte ponovo: ")
            else:
                print("Niste uneli poziciju u opsegu 0-7,0-2 ")
        except ValueError:
            print("Niste uneli broj kao mogucu poziciju, probajte ponovo: ")
    board.delete_piece(board_position)
    return board


def make_new_tree(board, depth, stage1, player):
    new_board = copy.deepcopy(board.array)
    root = Node(new_board)
    tree = Tree(root)
    return configure_whole_tree(tree, player, depth, stage1)


def configure_whole_tree(tree, player, depth, stage1):
    red = queue()
    red.enqueue(tree.root)
    current_node = tree.root
    while not red.is_empty():
        element = red.dequeue()
        if element.depth != current_node.depth:
            player = change_player(player)
        current_node = element
        if stage1:
            possible_moves = make_possible_moves_from_board(element.board, player)
        else:
            possible_moves = make_possible_moves_stage23(element.board, change_player(player))
        for item in possible_moves:
            new_node = Node(item)
            tree.set_children(new_node, current_node)
        if new_node.depth == depth - 1:
            continue
        for child in element.get_children():
            red.enqueue(child)
    return tree


def change_player(player):
    if player == "W":
        return "B"
    return "W"


def make_possible_moves_from_board(board, player):
    list_of_possible_moves = []
    for i in range(24):
        if board[i] == "O":
            new_board = copy.deepcopy(board)
            if player == "B":
                new_board[i] = "W"
                if is_mill(new_board, "W", i):
                    list_of_possible_moves = remove_piece(new_board, list_of_possible_moves, "B")
                else:
                    list_of_possible_moves.append(new_board)
            else:
                new_board[i] = "B"
                if is_mill(new_board, "B", i):
                    list_of_possible_moves = remove_piece(new_board, list_of_possible_moves, "W")
                else:
                    list_of_possible_moves.append(new_board)
    return list_of_possible_moves


def make_possible_moves_stage23(board, player):
    list_of_possible_moves = []
    for i in range(len(board)):
        if board[i] == player:
            if number_of_pieces_on_board(board, player) > 3:
                new_places = find_free_surrounding_place(board, i)
            else:
                new_places = find_all_free_places(board)
            for position in new_places:
                new_board = copy.deepcopy(board)
                new_board[i] = "O"
                new_board[position] = player
                if is_mill(new_board, player, position):
                    list_of_possible_moves = remove_piece(new_board, list_of_possible_moves, change_player(player))
                else:
                    list_of_possible_moves.append(new_board)
    return list_of_possible_moves


def find_depth(board, stage1):
    number_of_figurines = 0
    for item in board.array:
        if item != "O":
            number_of_figurines += 1
    if stage1 or (number_of_pieces_on_board(board.array, "W") > 3 and number_of_pieces_on_board(board.array, "B") > 3):
        if number_of_figurines < 4:
            return 3
        elif number_of_figurines < 14:
            return 4
        return 5
    else:
        return 2


def remove_piece(board, board_list, removing_this_player):
    for i in range(len(board)):
        if board[i] == removing_this_player:
            if all_are_mill(board, removing_this_player):
                new_board = copy.deepcopy(board)
                new_board[i] = "O"
                board_list.append(new_board)
            elif not is_mill(board, removing_this_player, i):
                new_board = copy.deepcopy(board)
                new_board[i] = "O"
                board_list.append(new_board)
    return board_list


def evaluate_board(node, stage1):
    mill = 0
    free_space = 0
    close_mill = 0
    difference_between_pieces = 0
    double_morris = 0
    close_double_morris = 0
    closed_mills_before = mill_in_last_move(node)
    winner = 0
    mill_b = 0
    for i in range(len(node.board)):
        if node.board[i] == "W":
            difference_between_pieces += 1
            if is_mill(node.board, "W", i):
                mill += 1
            if not free_space_around(node.board, "W"):
                free_space -= 1
            if is_close_for_mill(node.board, "W", i):
                close_mill += 1
            if is_double_morris(node.board, "W", i):
                double_morris += 1
            if is_close_for_double_morris(node.board, "W", i):
                close_double_morris += 1
        elif node.board[i] == "B":
            difference_between_pieces -= 1
            if is_mill(node.board, "B", i):
                mill -= 1
                mill_b -= 1
            if not free_space_around(node.board, "B"):
                free_space += 1
            if is_close_for_mill(node.board, "B", i):
                close_mill -= 1
            if is_double_morris(node.board, "B", i):
                double_morris -= 1
            if is_close_for_double_morris(node.board, "B", i):
                close_double_morris -= 1
    if not stage1:
        if number_of_pieces_on_board(node.board, "W") == 2:
            winner = -1
        if number_of_pieces_on_board(node.board, "B") == 2:
            winner = 1
    if stage1:
        return 18 * closed_mills_before + 26 * mill + free_space + 9 * difference_between_pieces + 10 * close_mill + \
               7 * close_double_morris
    elif number_of_pieces_on_board(node.board, "W") > 3 and number_of_pieces_on_board(node.board, "B") > 3:
        return 14 * closed_mills_before + 43 * mill + 10 * free_space + 11 * difference_between_pieces + 8 *\
               double_morris + 1086 * winner
    else:
        if number_of_pieces_on_board(node.board, "B") == 3:
            return 16 * closed_mills_before + 10 * close_mill + close_double_morris + 1190 * winner + 3 * mill_b
        return 16 * closed_mills_before + 10 * close_mill + close_double_morris + 1190 * winner


def is_end(board, player):
    if number_of_pieces_on_board(board, player) <= 2:
        return True
    for i in range(len(board)):
        if board[i] == player:
            if not no_free_position_left(board, i):
                return False
    return True


def find_all_free_places(board):
    list_of_free_positions = []
    for i in range(len(board)):
        if board[i] == "O":
            list_of_free_positions.append(i)
    return list_of_free_positions


def find_free_surrounding_place(board, position):
    free_places = []
    potential_places = moving_accessibility_positions(position)
    for item in potential_places:
        if board[item] == "O":
            free_places.append(item)
    return free_places


def positions_which_form_mill(position):
    combinations = [
        [[0, 1, 2], [0, 9, 21]],
        [[0, 1, 2], [1, 4, 7]],
        [[2, 1, 0], [2, 14, 23]],
        [[3, 4, 5], [3, 10, 18]],
        [[3, 4, 5], [4, 7, 1]],
        [[5, 4, 3], [5, 13, 20]],
        [[6, 11, 15], [6, 7, 8]],
        [[6, 7, 8], [7, 4, 1]],
        [[8, 7, 6], [8, 12, 17]],
        [[0, 9, 21], [9, 10, 11]],
        [[3, 10, 18], [9, 10, 11]],
        [[6, 11, 15], [9, 10, 11]],
        [[8, 12, 17], [12, 13, 14]],
        [[5, 13, 20], [12, 13, 14]],
        [[12, 13, 14], [2, 14, 23]],
        [[6, 11, 15], [15, 16, 17]],
        [[15, 16, 17], [16, 19, 22]],
        [[15, 16, 17], [8, 12, 17]],
        [[3, 10, 18], [18, 19, 20]],
        [[18, 19, 20], [16, 19, 22]],
        [[18, 19, 20], [5, 13, 20]],
        [[0, 9, 21], [21, 22, 23]],
        [[21, 22, 23], [16, 19, 22]],
        [[21, 22, 23], [2, 14, 23]],
    ]
    return combinations[position]


def moving_accessibility_positions(position):
    combinations = [
        [1, 9],
        [0, 2, 4],
        [1, 14],
        [4, 10],
        [3, 5, 7, 1],
        [4, 13],
        [11, 7],
        [6, 8, 4],
        [7, 12],
        [0, 21, 10],
        [9, 11, 3, 18],
        [10, 6, 15],
        [8, 17, 13],
        [5, 20, 12, 14],
        [2, 23, 13],
        [11, 16],
        [15, 17, 19],
        [12, 16],
        [10, 19],
        [18, 20, 16, 22],
        [13, 19],
        [9, 22],
        [21, 23, 19],
        [14, 22]
    ]
    return combinations[position]


def is_close_for_mill(board, player, key):
    if board[key] != player:
        return False
    possible_mills = positions_which_form_mill(key)
    for formation in possible_mills:
        if close_mill_condition(board, formation, player):
            return True
    return False


def close_mill_condition(board, formation, player):
    if board[formation[0]] == player and board[formation[1]] == player and board[formation[2]] == "O":
        return True
    elif board[formation[0]] == player and board[formation[2]] == player and board[formation[1]] == "O":
        return True
    elif board[formation[1]] == player and board[formation[2]] == player and board[formation[0]] == "O":
        return True
    return False


def free_space_around(board, position):
    if position == 0:
        if board[1] == "O" or board[9] == "O":
            return True
    elif position == 1:
        if board[0] == "O" or board[2] == "O" or board[4] == "O":
            return True
    elif position == 2:
        if board[1] == "O" or board[14] == "O":
            return True
    elif position == 3:
        if board[10] == "O" or board[4] == "O":
            return True
    elif position == 4:
        if board[3] == "O" or board[1] == "O" or board[5] == "O" or board[7] == "O":
            return True
    elif position == 5:
        if board[4] == "O" or board[13] == "O":
            return True
    elif position == 6:
        if board[11] == "O" or board[7] == "O":
            return True
    elif position == 7:
        if board[6] == "O" or board[8] == "O" or board[4] == "O":
            return True
    elif position == 8:
        if board[12] == "O" or board[7] == "O":
            return True
    elif position == 9:
        if board[0] == "O" or board[10] == "O" or board[21] == "O":
            return True
    elif position == 10:
        if board[3] == "O" or board[9] == "O" or board[18] == "O" or board[11] == "O":
            return True
    elif position == 11:
        if board[6] == "O" or board[15] == "O" or board[10] == "O":
            return True
    elif position == 12:
        if board[8] == "O" or board[17] == "O" or board[13] == "O":
            return True
    elif position == 13:
        if board[12] == "O" or board[14] == "O" or board[20] or board[5] == "O":
            return True
    elif position == 14:
        if board[2] == "O" or board[13] == "O" or board[23] == "O":
            return True
    elif position == 15:
        if board[11] == "O" or board[6] == "O":
            return True
    elif position == 16:
        if board[15] == "O" or board[17] == "O" or board[19] == "O":
            return True
    elif position == 17:
        if board[12] == "O" or board[16] == "O":
            return True
    elif position == 18:
        if board[10] == "O" or board[19] == "O":
            return True
    elif position == 19:
        if board[18] == "O" or board[20] == "O" or board[16] == "O" or board[22] == "O":
            return True
    elif position == 20:
        if board[13] == "O" or board[19] == "O":
            return True
    elif position == 21:
        if board[9] == "O" or board[22] == "O":
            return True
    elif position == 22:
        if board[21] == "O" or board[23] == "O" or board[19] == "O":
            return True
    elif position == 23:
        if board[22] == "O" or board[14] == "O":
            return True
    return False


def is_mill(board, player, key):
    possible_mills = positions_which_form_mill(key)
    for formation in possible_mills:
        if mill_condition(board, formation, player):
            return True
    return False


def mill_condition(board, formation, player):
    return board[formation[0]] == player and board[formation[1]] == player and board[formation[2]] == player


def all_are_mill(board, player):
    for i in range(len(board)):
        if board[i] == player:
            if not is_mill(board, player, i):
                return False
    return True


def mill_in_last_move(node):
    try:
        board_before_last_move = node.parent.parent.board
        board_last_move = node.parent.board
        key = find_difference_between_boards(board_before_last_move, board_last_move)
        if is_mill(board_last_move, "W", key):
            return 1
        elif is_mill(board_last_move, "B", key):
            return -1
        return 0
    except AttributeError:
        return 0


def accessible_position(old_position, new_position):
    return new_position in moving_accessibility_positions(old_position)


def no_free_position_left(board, position):
    possible_free_places = moving_accessibility_positions(position)
    for position_next_to in possible_free_places:
        if board[position_next_to] == "O":
            return False
    return True


def number_of_pieces_on_board(board, player):
    number_of_pieces = 0
    for i in range(len(board)):
        if board[i] == player:
            number_of_pieces += 1
    return number_of_pieces


def is_double_morris(board, player, key):
    possible_mills = positions_which_form_mill(key)
    formation_one = possible_mills[0]
    formation_two = possible_mills[1]
    if mill_condition(board, formation_one, player) and mill_condition(board, formation_two, player):
        return True
    return False


def is_close_for_double_morris(board, player, key):
    possible_mills = positions_which_form_mill(key)
    formation_one = possible_mills[0]
    formation_two = possible_mills[1]
    if close_mill_condition(board, formation_one, player) and close_mill_condition(board, formation_two, player):
        return True
    return False


def find_difference_between_boards(old_board, new_board):
    for i in range(len(old_board)):
        if old_board[i] != new_board[i]:
            return i


def hes(key):
    hashed_key = int(key[0]) * 3 + int(key[1])
    return hashed_key


class Tree(object):
    def __init__(self, node):
        self.root = node

    @staticmethod
    def set_children(node, parent):
        parent.children.append(node)
        node.parent = parent
        node.depth = parent.depth + 1


class Node(object):
    def __init__(self, board):
        self.parent = None
        self.children = []
        self.depth = 0
        self.board = board

    def get_children(self):
        return self.children


class queue:
    def __init__(self):
        self._data = []

    def enqueue(self, element):
        self._data.append(element)

    def dequeue(self):
        if len(self._data) == 0:
            raise QueueException("Prazan red, nema sta izvuci.")
        for item in self._data:
            element = item
            self._data.remove(item)
            break
        return element

    def first(self):
        if len(self._data) == 0:
            raise QueueException("Prazan red, nema sta izvuci.")
        return self._data[0]

    def __len__(self):
        return len(self._data)

    def is_empty(self):
        return len(self._data) == 0


class QueueException(Exception):
    pass


class HashMap(object):
    def __init__(self):
        self.array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    def dodaj(self, key, value):
        self.array[hes(key)] = value

    def obrisi(self, key):
        if self.array[hes(key)] != 0:
            self.array[hes(key)] = 0
            return True
        else:
            return False

    def pristupi(self, key):
        return self.array[hes(key)]

    def __str__(self):
        output = ""
        output += str(self.array[0]) + "--" + str(self.array[1]) + "--" + str(self.array[2]) + "\n"
        output += "|" + str(self.array[3]) + "-" + str(self.array[4]) + "-" + str(self.array[5]) + "|\n"
        output += "||" + str(self.array[6]) + str(self.array[7]) + str(self.array[8]) + "||\n"
        output += str(self.array[9]) + str(self.array[10]) + str(self.array[11])
        output += "*" + str(self.array[12]) + str(self.array[13]) + str(self.array[14]) + "\n"
        output += "||" + str(self.array[15]) + str(self.array[16]) + str(self.array[17]) + "||\n"
        output += "|" + str(self.array[18]) + "-" + str(self.array[19]) + "-" + str(self.array[20]) + "|\n"
        output += str(self.array[21]) + "--" + str(self.array[22]) + "--" + str(self.array[23]) + "\n"
        return output


if __name__ == '__main__':
    player_mark = input().strip()
    action = input().strip()
    board_strs = ""
    for i in range(7):
        board_strs += input().strip()
    print(nextMove(player_mark, action, board_strs))
