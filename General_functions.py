import copy
from Tree import *
from red import queue
from HashMap import hes
from Mill import number_of_pieces_on_board, no_free_position_left, moving_accessibility_positions,\
    is_close_for_double_morris, is_double_morris, all_are_mill, mill_in_last_move, is_mill, free_space_around, \
    is_close_for_mill


def remove_figurine(board):
    while True:
        board_position = input("Molimo Vas unesite poziciju figure koju zelite da sklonite: ")
        try:
            if len(board_position) == 2 and 0 <= int(board_position[0]) < 8 and 0 <= int(board_position[1]) < 3:
                if board.get_piece(board_position) == "B":
                    if all_are_mill(board.array, "B"):
                        break
                    elif not is_mill(board.array, "B", hes(board_position)):
                        break
                else:
                    print("Nevazece mesto, probajte ponovo: ")
            else:
                print("Niste uneli poziciju u opsegu 0-7,0-2 ")
        except ValueError:
            print("Niste uneli broj kao mogucu poziciju, probajte ponovo: ")
    board.delete_piece(board_position)
    return board


def make_new_tree(board, depth, stage1):
    new_board = copy.deepcopy(board.array)
    root = Node(new_board)
    tree = Tree(root)
    return configure_whole_tree(tree, "W", depth, stage1)


def configure_whole_tree(tree, player, depth, stage1):
    red = queue()
    red.enqueue(tree.root)
    current_node = tree.root
    while not red.is_empty():
        element = red.dequeue()
        if element.depth != current_node.depth:
            player = change_player(player)
        current_node = element
        if stage1:
            possible_moves = make_possible_moves_from_board(element.board, player)
        else:
            possible_moves = make_possible_moves_stage23(element.board, change_player(player))
        for item in possible_moves:
            new_node = Node(item)
            tree.set_children(new_node, current_node)
        if new_node.depth == depth - 1:
            continue
        for child in element.get_children():
            red.enqueue(child)
    return tree


def change_player(player):
    if player == "W":
        return "B"
    return "W"


def make_possible_moves_from_board(board, player):
    list_of_possible_moves = []
    for i in range(24):
        if board[i] == 0:
            new_board = copy.deepcopy(board)
            if player == "B":
                new_board[i] = "W"
                if is_mill(new_board, "W", i):
                    list_of_possible_moves = remove_piece(new_board, list_of_possible_moves, "B")
                else:
                    list_of_possible_moves.append(new_board)
            else:
                new_board[i] = "B"
                if is_mill(new_board, "B", i):
                    list_of_possible_moves = remove_piece(new_board, list_of_possible_moves, "W")
                else:
                    list_of_possible_moves.append(new_board)
    return list_of_possible_moves


def make_possible_moves_stage23(board, player):
    list_of_possible_moves = []
    for i in range(len(board)):
        if board[i] == player:
            if number_of_pieces_on_board(board, player) > 3:
                new_places = find_free_surrounding_place(board, i)
            else:
                new_places = find_all_free_places(board)
            for position in new_places:
                new_board = copy.deepcopy(board)
                new_board[i] = 0
                new_board[position] = player
                if is_mill(new_board, player, position):
                    list_of_possible_moves = remove_piece(new_board, list_of_possible_moves, change_player(player))
                else:
                    list_of_possible_moves.append(new_board)
    return list_of_possible_moves


def find_depth(board, stage1):
    number_of_figurines = 0
    for item in board.array:
        if item != 0:
            number_of_figurines += 1
    if stage1 or (number_of_pieces_on_board(board.array, "W") > 3 and number_of_pieces_on_board(board.array, "B") > 3):
        if number_of_figurines < 4:
            return 3
        elif number_of_figurines < 14:
            return 4
        return 5
    else:
        return 2


def remove_piece(board, board_list, removing_this_player):
    for i in range(len(board)):
        if board[i] == removing_this_player:
            if all_are_mill(board, removing_this_player):
                new_board = copy.deepcopy(board)
                new_board[i] = 0
                board_list.append(new_board)
            elif not is_mill(board, removing_this_player, i):
                new_board = copy.deepcopy(board)
                new_board[i] = 0
                board_list.append(new_board)
    return board_list


def evaluate_board(node, stage1):
    mill = 0
    free_space = 0
    close_mill = 0
    difference_between_pieces = 0
    double_morris = 0
    close_double_morris = 0
    closed_mills_before = mill_in_last_move(node)
    winner = 0
    mill_b = 0
    for i in range(len(node.board)):
        if node.board[i] == "W":
            difference_between_pieces += 1
            if is_mill(node.board, "W", i):
                mill += 1
            if not free_space_around(node.board, "W"):
                free_space -= 1
            if is_close_for_mill(node.board, "W", i):
                close_mill += 1
            if is_double_morris(node.board, "W", i):
                double_morris += 1
            if is_close_for_double_morris(node.board, "W", i):
                close_double_morris += 1
        elif node.board[i] == "B":
            difference_between_pieces -= 1
            if is_mill(node.board, "B", i):
                mill -= 1
                mill_b -= 1
            if not free_space_around(node.board, "B"):
                free_space += 1
            if is_close_for_mill(node.board, "B", i):
                close_mill -= 1
            if is_double_morris(node.board, "B", i):
                double_morris -= 1
            if is_close_for_double_morris(node.board, "B", i):
                close_double_morris -= 1
    if not stage1:
        if number_of_pieces_on_board(node.board, "W") == 2:
            winner = -1
        if number_of_pieces_on_board(node.board, "B") == 2:
            winner = 1
    if stage1:
        return 18 * closed_mills_before + 26 * mill + free_space + 9 * difference_between_pieces + 10 * close_mill + \
               7 * close_double_morris
    elif number_of_pieces_on_board(node.board, "W") > 3 and number_of_pieces_on_board(node.board, "B") > 3:
        return 14 * closed_mills_before + 43 * mill + 10 * free_space + 11 * difference_between_pieces + 8 *\
               double_morris + 1086 * winner
    else:
        if number_of_pieces_on_board(node.board, "B") == 3:
            return 16 * closed_mills_before + 10 * close_mill + close_double_morris + 1190 * winner + 3 * mill_b
        return 16 * closed_mills_before + 10 * close_mill + close_double_morris + 1190 * winner


def is_end(board, player):
    if number_of_pieces_on_board(board, player) <= 2:
        return True
    for i in range(len(board)):
        if board[i] == player:
            if not no_free_position_left(board, i):
                return False
    return True


def find_all_free_places(board):
    list_of_free_positions = []
    for i in range(len(board)):
        if board[i] == 0:
            list_of_free_positions.append(i)
    return list_of_free_positions


def find_free_surrounding_place(board, position):
    free_places = []
    potential_places = moving_accessibility_positions(position)
    for item in potential_places:
        if board[item] == 0:
            free_places.append(item)
    return free_places
