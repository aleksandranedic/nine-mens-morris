def hes(key):
    hashed_key = int(key[0]) * 3 + int(key[1])
    return hashed_key


class HashMap(object):
    def __init__(self):
        self.array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    def add_piece(self, key, value):
        self.array[hes(key)] = value

    def delete_piece(self, key):
        if self.array[hes(key)] != 0:
            self.array[hes(key)] = 0
            return True
        else:
            return False

    def get_piece(self, key):
        return self.array[hes(key)]

    def __str__(self):
        output = ""
        output += str(self.array[0]) + "--" + str(self.array[1]) + "--" + str(self.array[2]) + "\n"
        output += "|" + str(self.array[3]) + "-" + str(self.array[4]) + "-" + str(self.array[5]) + "|\n"
        output += "||" + str(self.array[6]) + str(self.array[7]) + str(self.array[8]) + "||\n"
        output += str(self.array[9]) + str(self.array[10]) + str(self.array[11])
        output += "*" + str(self.array[12]) + str(self.array[13]) + str(self.array[14]) + "\n"
        output += "||" + str(self.array[15]) + str(self.array[16]) + str(self.array[17]) + "||\n"
        output += "|" + str(self.array[18]) + "-" + str(self.array[19]) + "-" + str(self.array[20]) + "|\n"
        output += str(self.array[21]) + "--" + str(self.array[22]) + "--" + str(self.array[23]) + "\n"
        return output
