class queue:
    def __init__(self):
        self._data = []

    def enqueue(self, element):
        self._data.append(element)

    def dequeue(self):
        if len(self._data) == 0:
            raise QueueException("Prazan red, nema sta izvuci.")
        for item in self._data:
            element = item
            self._data.remove(item)
            break
        return element

    def first(self):
        if len(self._data) == 0:
            raise QueueException("Prazan red, nema sta izvuci.")
        return self._data[0]

    def __len__(self):
        return len(self._data)

    def is_empty(self):
        return len(self._data) == 0


class QueueException(Exception):
    pass
