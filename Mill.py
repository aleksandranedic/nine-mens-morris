def positions_which_form_mill(position):
    combinations = [
        [[0, 1, 2], [0, 9, 21]],
        [[0, 1, 2], [1, 4, 7]],
        [[2, 1, 0], [2, 14, 23]],
        [[3, 4, 5], [3, 10, 18]],
        [[3, 4, 5], [4, 7, 1]],
        [[5, 4, 3], [5, 13, 20]],
        [[6, 11, 15], [6, 7, 8]],
        [[6, 7, 8], [7, 4, 1]],
        [[8, 7, 6], [8, 12, 17]],
        [[0, 9, 21], [9, 10, 11]],
        [[3, 10, 18], [9, 10, 11]],
        [[6, 11, 15], [9, 10, 11]],
        [[8, 12, 17], [12, 13, 14]],
        [[5, 13, 20], [12, 13, 14]],
        [[12, 13, 14], [2, 14, 23]],
        [[6, 11, 15], [15, 16, 17]],
        [[15, 16, 17], [16, 19, 22]],
        [[15, 16, 17], [8, 12, 17]],
        [[3, 10, 18], [18, 19, 20]],
        [[18, 19, 20], [16, 19, 22]],
        [[18, 19, 20], [5, 13, 20]],
        [[0, 9, 21], [21, 22, 23]],
        [[21, 22, 23], [16, 19, 22]],
        [[21, 22, 23], [2, 14, 23]],
    ]
    return combinations[position]


def moving_accessibility_positions(position):
    combinations = [
        [1, 9],
        [0, 2, 4],
        [1, 14],
        [4, 10],
        [3, 5, 7, 1],
        [4, 13],
        [11, 7],
        [6, 8, 4],
        [7, 12],
        [0, 21, 10],
        [9, 11, 3, 18],
        [10, 6, 15],
        [8, 17, 13],
        [5, 20, 12, 14],
        [2, 23, 13],
        [11, 16],
        [15, 17, 19],
        [12, 16],
        [10, 19],
        [18, 20, 16, 22],
        [13, 19],
        [9, 22],
        [21, 23, 19],
        [14, 22]
    ]
    return combinations[position]


def is_close_for_mill(board, player, key):
    if board[key] != player:
        return False
    possible_mills = positions_which_form_mill(key)
    for formation in possible_mills:
        if close_mill_condition(board, formation, player):
            return True
    return False


def close_mill_condition(board, formation, player):
    if board[formation[0]] == player and board[formation[1]] == player and board[formation[2]] == 0:
        return True
    elif board[formation[0]] == player and board[formation[2]] == player and board[formation[1]] == 0:
        return True
    elif board[formation[1]] == player and board[formation[2]] == player and board[formation[0]] == 0:
        return True
    return False


def free_space_around(board, position):
    if position == 0:
        if board[1] == 0 or board[9] == 0:
            return True
    elif position == 1:
        if board[0] == 0 or board[2] == 0 or board[4] == 0:
            return True
    elif position == 2:
        if board[1] == 0 or board[14] == 0:
            return True
    elif position == 3:
        if board[10] == 0 or board[4] == 0:
            return True
    elif position == 4:
        if board[3] == 0 or board[1] == 0 or board[5] == 0 or board[7] == 0:
            return True
    elif position == 5:
        if board[4] == 0 or board[13] == 0:
            return True
    elif position == 6:
        if board[11] == 0 or board[7] == 0:
            return True
    elif position == 7:
        if board[6] == 0 or board[8] == 0 or board[4] == 0:
            return True
    elif position == 8:
        if board[12] == 0 or board[7] == 0:
            return True
    elif position == 9:
        if board[0] == 0 or board[10] == 0 or board[21] == 0:
            return True
    elif position == 10:
        if board[3] == 0 or board[9] == 0 or board[18] == 0 or board[11] == 0:
            return True
    elif position == 11:
        if board[6] == 0 or board[15] == 0 or board[10] == 0:
            return True
    elif position == 12:
        if board[8] == 0 or board[17] == 0 or board[13] == 0:
            return True
    elif position == 13:
        if board[12] == 0 or board[14] == 0 or board[20] or board[5] == 0:
            return True
    elif position == 14:
        if board[2] == 0 or board[13] == 0 or board[23] == 0:
            return True
    elif position == 15:
        if board[11] == 0 or board[6] == 0:
            return True
    elif position == 16:
        if board[15] == 0 or board[17] == 0 or board[19] == 0:
            return True
    elif position == 17:
        if board[12] == 0 or board[16] == 0:
            return True
    elif position == 18:
        if board[10] == 0 or board[19] == 0:
            return True
    elif position == 19:
        if board[18] == 0 or board[20] == 0 or board[16] == 0 or board[22] == 0:
            return True
    elif position == 20:
        if board[13] == 0 or board[19] == 0:
            return True
    elif position == 21:
        if board[9] == 0 or board[22] == 0:
            return True
    elif position == 22:
        if board[21] == 0 or board[23] == 0 or board[19] == 0:
            return True
    elif position == 23:
        if board[22] == 0 or board[14] == 0:
            return True
    return False


def is_mill(board, player, key):
    possible_mills = positions_which_form_mill(key)
    for formation in possible_mills:
        if mill_condition(board, formation, player):
            return True
    return False


def mill_condition(board, formation, player):
    return board[formation[0]] == player and board[formation[1]] == player and board[formation[2]] == player


def all_are_mill(board, player):
    for i in range(len(board)):
        if board[i] == player:
            if not is_mill(board, player, i):
                return False
    return True


def mill_in_last_move(node):
    try:
        board_before_last_move = node.parent.parent.board
        board_last_move = node.parent.board
        key = find_difference_between_boards(board_before_last_move, board_last_move)
        if is_mill(board_last_move, "W", key):
            return 1
        elif is_mill(board_last_move, "B", key):
            return -1
        return 0
    except AttributeError:
        return 0


def accessible_position(old_position, new_position):
    return new_position in moving_accessibility_positions(old_position)


def no_free_position_left(board, position):
    possible_free_places = moving_accessibility_positions(position)
    for position_next_to in possible_free_places:
        if board[position_next_to] == 0:
            return False
    return True


def number_of_pieces_on_board(board, player):
    number_of_pieces = 0
    for i in range(len(board)):
        if board[i] == player:
            number_of_pieces += 1
    return number_of_pieces


def is_double_morris(board, player, key):
    possible_mills = positions_which_form_mill(key)
    formation_one = possible_mills[0]
    formation_two = possible_mills[1]
    if mill_condition(board, formation_one, player) and mill_condition(board, formation_two, player):
        return True
    return False


def is_close_for_double_morris(board, player, key):
    possible_mills = positions_which_form_mill(key)
    formation_one = possible_mills[0]
    formation_two = possible_mills[1]
    if close_mill_condition(board, formation_one, player) and close_mill_condition(board, formation_two, player):
        return True
    return False


def find_difference_between_boards(old_board, new_board):
    for i in range(len(old_board)):
        if old_board[i] != new_board[i]:
            return i
