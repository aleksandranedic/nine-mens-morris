class Tree(object):
    def __init__(self, node):
        self.root = node

    @staticmethod
    def set_children(node, parent):
        parent.children.append(node)
        node.parent = parent
        node.depth = parent.depth + 1


class Node(object):
    def __init__(self, board):
        self.parent = None
        self.children = []
        self.depth = 0
        self.board = board

    def get_children(self):
        return self.children
